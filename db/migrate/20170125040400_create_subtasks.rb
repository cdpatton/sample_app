class CreateSubtasks < ActiveRecord::Migration
  def change
    create_table :subtasks do |t|
      t.string :name
      t.integer :number
      t.datetime :due_at
      t.datetime :completed_on
      t.text :description
      t.references :task, index: true

      t.timestamps null: false
    end
    add_foreign_key :subtasks, :tasks
  end
end
