class ChangeCompletedAtToCompletedOnOnTasks < ActiveRecord::Migration
  def change
    remove_column :tasks, :completed_at, :datetime
    add_column :tasks, :completed_on, :datetime
  end
end
