require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  def setup
    @user = users(:archer)
    @project = @user.projects.build(name: "A Collection of Tasks", 
                           created_at: Time.zone.now, due_at: Time.zone.now + 1.week,
                           completed_on: nil, 
                           description: "A plan to accomplish by next week")
  end
  
  
  test "should be valid" do
    assert @project.valid?
  end
  
  test "user ID should be present" do
    @project.user_id = nil
    assert_not @project.valid?
  end
  
  test "name should be present and not longer than 30 characters" do
    @project.name = ""
    assert_not @project.valid?
    @project.name = "a" * 31
    assert_not @project.valid?
  end
  
end
