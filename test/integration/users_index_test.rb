require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @user = users(:archer)
  end

  test "index including pagination" do
    login_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    User.paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
    end
  end
  
  test "admin should see delete links and be able to delete users" do
    login_as(@admin)
    get users_path
    assert_template 'users/index'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete',
                       method: :delete
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@user)
    end
  end
  
  test "non-admin user should not see delete links" do
    login_as(@user)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end
end
