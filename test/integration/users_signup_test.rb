require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup should not create new record" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: {name: "",
                              email: "user@invalid",
                              password: "foo",
                              password_confirmation: "bar"}
    end
    assert_template 'users/new'
  end
  
    test "valid signup with account activation" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user: {name: "Example User",
                              email: "user@example.com",
                              password: "password",
                              password_confirmation: "password"}
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # login attempt before activating account
    login_as(user)
    assert_not is_logged_in?
    # activation token doesn't match email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # correct token and email activates account
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    # once activated, user redirected to his page and logged in
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end
